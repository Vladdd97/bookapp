package com.awesomebooks.bookApp.controllers;

import com.awesomebooks.bookApp.commands.BookCommand;
import com.awesomebooks.bookApp.converters.BookConverter;
import com.awesomebooks.bookApp.models.Book;
import com.awesomebooks.bookApp.services.BookServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BookRestController {


    @Autowired
    private BookServiceImpl bookServiceImpl;
    @Autowired
    private BookConverter bookConverter;


    @GetMapping("book/getAllBooks2")
    public Iterable<Book> getAllBooks(Model model){

        Iterable<Book> books = bookServiceImpl.getAllBooks();
        return books;
    }


}
