package com.awesomebooks.bookApp.commands;


public class BookCommand {

    private Long id;

    private String title;

    private Integer numberOfPages;

    private String author;


    public BookCommand() {
    }

    public BookCommand(Long id, String title, Integer numberOfPages, String author) {
        this.id = id;
        this.title = title;
        this.numberOfPages = numberOfPages;
        this.author = author;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(Integer numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }


}
