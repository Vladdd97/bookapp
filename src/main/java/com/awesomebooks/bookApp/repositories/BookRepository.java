package com.awesomebooks.bookApp.repositories;

import com.awesomebooks.bookApp.models.Book;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository <Book,Long> {
}
