package com.awesomebooks.bookApp.controllers;


import com.awesomebooks.bookApp.commands.BookCommand;
import com.awesomebooks.bookApp.converters.BookConverter;
import com.awesomebooks.bookApp.services.BookServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Controller
public class BookController {

    @Autowired
    private BookServiceImpl bookServiceImpl;
    @Autowired
    private BookConverter bookConverter;


    @GetMapping("/")
    public String index(){

//        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
//        String password = "vasile";
//        System.out.println("password : " + password);
//        System.out.println("PASSWORD : " + bCryptPasswordEncoder.encode(password));

        return "index";
    }

    @GetMapping("/userdetails")
    public String userdetails(Model model , Principal principal){
        //Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
       // model.addAttribute("userDetails",authentication.getName());
        model.addAttribute("userDetails",principal);
       // model.addAttribute("userDetails","userDetails");
        return "userdetails";
    }

    @GetMapping("book/getAllBooks")
    public String getAllBooks(Model model){

        model.addAttribute("books",bookServiceImpl.getAllBooks());
        return "book/getAllBooks";
    }

    @GetMapping("book/bookForm")
    public String bookForm(Model model){

        model.addAttribute("book",new BookCommand());
        return "book/bookForm";
    }

    @PostMapping("/book/saveOrUpdate")
    public String saveOrUpdate( @ModelAttribute BookCommand bookCommand){
        bookServiceImpl.save( bookConverter.BookCommandToBook(bookCommand) );
        return "redirect:/book/getAllBooks";
    }

    @GetMapping("book/updateBook")
    public String updateBook (@RequestParam("bookId") String id, Model model){
        model.addAttribute("book", bookConverter.BookToBookCommand( bookServiceImpl.findById(Long.valueOf(id)) ));
        return  "book/bookForm";
    }

    @GetMapping("book/deleteBook")
    public String deleteBook (@RequestParam("bookId") String id){
        bookServiceImpl.deleteById(Long.valueOf(id));
        return  "redirect:/book/getAllBooks";
    }
}
