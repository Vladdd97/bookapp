package com.awesomebooks.bookApp.converters;


import com.awesomebooks.bookApp.commands.BookCommand;
import com.awesomebooks.bookApp.models.Book;
import org.springframework.stereotype.Component;

@Component
public class BookConverter {

    public BookCommand BookToBookCommand(Book book){
        if ( book == null)
            return null;

        BookCommand bookCommand = new BookCommand();
        bookCommand.setId(book.getId());
        bookCommand.setAuthor(book.getAuthor());
        bookCommand.setTitle(book.getTitle());
        bookCommand.setNumberOfPages(book.getNumberOfPages());
        return bookCommand;
    }

    public Book BookCommandToBook (BookCommand bookCommand){
        if(bookCommand == null)
            return null;

        Book book = new Book();
        book.setId(bookCommand.getId());
        book.setAuthor(bookCommand.getAuthor());
        book.setTitle(bookCommand.getTitle());
        book.setNumberOfPages(bookCommand.getNumberOfPages());
        return book;
    }

}
