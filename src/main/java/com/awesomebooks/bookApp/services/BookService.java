package com.awesomebooks.bookApp.services;

import com.awesomebooks.bookApp.models.Book;

import java.util.Set;

public interface BookService {

    Iterable<Book> getAllBooks();

    Book findById(Long l);

    Book save(Book book);

    void deleteById(Long id);

    Long countBook();




}
