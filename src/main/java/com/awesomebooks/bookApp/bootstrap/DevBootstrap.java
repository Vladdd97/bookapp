package com.awesomebooks.bookApp.bootstrap;

import com.awesomebooks.bookApp.commands.BookCommand;
import com.awesomebooks.bookApp.converters.BookConverter;
import com.awesomebooks.bookApp.models.Book;
import com.awesomebooks.bookApp.services.BookServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {


    @Autowired
    private BookServiceImpl bookServiceImpl;

    @Autowired
    private BookConverter bookConverter;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        initData();
    }

    public void initData() {

        // check if we have data in the book table
        // if there we don't have data then we initialize it , if we have data then we just skip initialization
        if ( bookServiceImpl.countBook() == 0 ) {
            ArrayList<BookCommand> bookCommands = new ArrayList<>();
            bookCommands.add(new BookCommand(Long.valueOf(100), "Assembler basic" , 45,"Metei Vasile"));
            bookCommands.add(new BookCommand(Long.valueOf(100), "C++" , 45,"Chirosca Ariadna"));
            bookCommands.add(new BookCommand(Long.valueOf(100), "C#" , 45,"Zaharia Gabriel"));
            bookCommands.add(new BookCommand(Long.valueOf(100), "Pascal" , 45,"Bantus Vladislav"));

            for (BookCommand bookCommand : bookCommands)
                bookServiceImpl.save(bookConverter.BookCommandToBook(bookCommand));
        }
        else{
            System.out.println("We have already data in our database !!!");
        }

    }
}
